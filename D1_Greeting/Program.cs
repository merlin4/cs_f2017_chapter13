﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace D1_Greeting
{
    public delegate void GreetingHandler(string name);

    class Program
    {
        static void Main(string[] args)
        {
            GreetingHandler hello = Hello;
            GreetingHandler goodbye = Goodbye;

            MagicGreeterBot(hello, "Cathy");
            MagicGreeterBot(goodbye, "Bob");
        }

        static void MagicGreeterBot(GreetingHandler handler, string name)
        {
            handler(name);
        }

        static void Hello(string name)
        {
            Console.WriteLine("Hello, {0}!", name);
        }

        static void Goodbye(string name)
        {
            Console.WriteLine("Goodbye, {0}!", name);
        }

        static void Am(string name)
        {
            Console.WriteLine("I am {0}.", name);
        }

        static void Like(string name)
        {
            Console.WriteLine("I like {0}.", name);
        }

        static void Love(string name)
        {
            Console.WriteLine("I love {0}.", name);
        }
    }
}
