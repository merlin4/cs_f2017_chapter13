﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace D4_HoverClick
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            label1.Text = "";
            label1.BackColor = Color.Transparent;
            label1.Font = new Font("Arial", 10.0f);
        }

        private void button1_MouseEnter(object sender, EventArgs e)
        {
            label1.Text = "Go ahead :)";
            label1.BackColor = Color.Transparent;
            label1.Font = new Font("Arial", 10.0f);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            label1.Text = "BOOM";
            label1.BackColor = Color.Red;
            label1.Font = new Font("Comic Sans MS", 32.0f);
        }

        private void button1_MouseLeave(object sender, EventArgs e)
        {
            label1.Text = "Awww :(";
            label1.BackColor = Color.Transparent;
            label1.Font = new Font("Arial", 10.0f);
        }
    }
}
