﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace D2_CompositeGreeting
{
    class Program
    {
        public delegate void GreetingHandler(string name);

        static void Main(string[] args)
        {
            GreetingHandler[] handlers = 
            {
                Hello, Goodbye, Am, Like, Love
            };
            string[] names =
            {
                "Cathy", "Bob", "Fred"
            };

            Random rand = new Random();
            for (;;)
            {
                string name = names[rand.Next(0, names.Length)];
                GreetingHandler h1 = handlers[rand.Next(0, handlers.Length)];
                GreetingHandler h2 = handlers[rand.Next(0, handlers.Length)];

                GreetingHandler composite = h1 + h2;
                composite(name);
                Console.ReadLine();
            }
        }

        static void Hello(string name)
        {
            Console.WriteLine("Hello, {0}!", name);
        }

        static void Goodbye(string name)
        {
            Console.WriteLine("Goodbye, {0}!", name);
        }

        static void Am(string name)
        {
            Console.WriteLine("I am {0}.", name);
        }

        static void Like(string name)
        {
            Console.WriteLine("I like {0}.", name);
        }

        static void Love(string name)
        {
            Console.WriteLine("I love {0}.", name);
        }
    }
}
