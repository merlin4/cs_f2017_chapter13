﻿using System;

namespace D3_StudentEvent
{
    public class Student
    {
        private int _id;
        private double _gpa;

        public event ChangedEventHandler Changed;

        private void OnChanged()
        {
            Changed(this, EventArgs.Empty);
        }

        public int Id
        {
            get { return _id; }
            set
            {
                _id = value;
                OnChanged();
            }
        }

        public double GPA
        {
            get { return _gpa; }
            set
            {
                _gpa = value;
                OnChanged();
            }
        }
    }
}
