﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace D3_StudentEvent
{
    class Program
    {
        static void Main(string[] args)
        {
            Student student = new Student();
            student.Changed += student_Changed;

            student.Id = 2345;
            student.Id = 4567;
            student.GPA = 3.2;
        }

        private static void student_Changed(object sender, EventArgs e)
        {
            Student student = (Student)sender;
            Console.WriteLine("The student has changed.");
            Console.WriteLine("ID# {0} GPA {1}", student.Id, student.GPA);
        }
    }
}
